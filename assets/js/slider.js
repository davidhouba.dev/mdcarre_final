var indexSlider = 0;
showSlider();

function showSlider() {
	var i;
	var slides = document.getElementsByClassName("mySlides");
	var dots = document.getElementsByClassName("dot");
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	indexSlider++;
	if (indexSlider > slides.length) {indexSlider = 1}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" active", "");
	}
	slides[indexSlider-1].style.display = "block";
	dots[indexSlider-1].className += " active";
	setTimeout(showSlider, 3500); // Change image every 2 seconds
}
