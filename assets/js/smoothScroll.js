$(document).ready(function(){
	//Ajouter l'effet de scroll sur tout les liens
	$("a").on('click', function(event) {

		//S'assure que this.hash à une valeur avant de remplacer le comportement par défault
		if (this.hash !== "") {
			// Empêche le comportement par défault du clic d'ancrage
			event.preventDefault();

			//stock le hash
			var hash = this.hash;


			//utilise la méthode jQuery animate() pour ajouter l'effet de scroll
			//Le nombre optionnel (1000) spécifie le nombre de millisecondes qu'il faut pour faire défiler la zone spécifiée.
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 1000, function(){

				//Ajoute le hash(#) à l'url lors du scrolling
				window.location.hash = hash;
			});
		}
	});
});

