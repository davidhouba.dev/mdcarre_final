// Ouvre le modal
function openModal() {
	document.getElementById('myModal').style.display = "block";
}

// Ferme le Modal
function closeModal() {
	document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// contrôle des boutons suivant/précédents
function plusSlides(n) {
	showSlides(slideIndex += n);
}

//contrôle de l'aperçus des images
function slideActif(n) {
	showSlides(slideIndex = n);
}


function showSlides(n) {
	var i;
	var slides = document.getElementsByClassName("monSlider");
	var dots = document.getElementsByClassName("galerie");

	if (n > slides.length) {slideIndex = 1}
	if (n < 1) {slideIndex = slides.length}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" active", "");
	}
	slides[slideIndex-1].style.display = "block";
	dots[slideIndex-1].className += " active";

}
