-- phpMyAdmin SQL Dump
-- version 3.5.8.1
-- http://www.phpmyadmin.net
--
-- Host: mdcarre.be.mysql:3306
-- Generation Time: Jun 05, 2019 at 01:26 PM
-- Server version: 10.1.30-MariaDB-1~xenial
-- PHP Version: 5.4.45-0+deb7u13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mdcarre_be`
--
CREATE DATABASE `mdcarre_be` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mdcarre_be`;

-- --------------------------------------------------------

--
-- Table structure for table `administrateur`
--

CREATE TABLE IF NOT EXISTS `administrateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `password` char(128) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `administrateur`
--

INSERT INTO `administrateur` (`id`, `email`, `password`) VALUES
(1, 'azerty@mail.com', '64f659726155b57f7565265402ca1545231a12d0dc153a285c8d543cabc7c69d127ce26c5126952af8239f368eb7ae3d9d86e04bb3c20991f32eb71acd5f5ba9');

-- --------------------------------------------------------

--
-- Table structure for table `contactez_nous`
--

CREATE TABLE IF NOT EXISTS `contactez_nous` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `contactez_nous`
--

INSERT INTO `contactez_nous` (`id`, `titre`, `text`) VALUES
(1, 'Modifier le numéro de téléphone', '0494/28 43 17'),
(2, 'Modifier l''adresse email', 'info@mdcarre.be'),
(3, 'Modifier l''adresse postale', 'MD² sprl Rue de Gembloux 500/10 5002 Saint-Servais'),
(4, 'Modifier les horaires', 'Du lundi au Vendredi de 8h00 à 18h00');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE IF NOT EXISTS `footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` text NOT NULL,
  `lien` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `titre`, `lien`, `text`) VALUES
(1, 'Modifier le premier lien', 'http://www.uniondesartisansdupatrimoine.be', 'Union des Artisans du Patrimoine'),
(2, 'Modifier le second lien', 'http://www.menuiserie-maquet.be', 'Menuiserie Maquet'),
(3, 'Modifier le troisième lien', 'http://www.bmotoiture.be', 'BMO Toiture');

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE IF NOT EXISTS `header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(255) NOT NULL,
  `alt_image` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`id`, `image_path`, `alt_image`) VALUES
(1, 'assets/images/header/porte.jpg', 'Porte'),
(2, 'assets/images/header/photo3(recadre).jpg', 'Terrasse'),
(3, 'assets/images/header/meuble1(recadre).jpg', 'Meuble');

-- --------------------------------------------------------

--
-- Table structure for table `qui_sommes_nous`
--

CREATE TABLE IF NOT EXISTS `qui_sommes_nous` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `alt_image` varchar(150) DEFAULT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `qui_sommes_nous`
--

INSERT INTO `qui_sommes_nous` (`id`, `titre`, `image_path`, `alt_image`, `text`) VALUES
(1, 'Qui sommes nous?', NULL, NULL, ''),
(11, 'Notre histoire', 'assets/images/section1/histoire(recadre).jpg', 'img_histoire', '<p>Notre <strong>menuiserie</strong> est née le 7 octobre 2014,de l''association des compétences techniques de François à celles de gestion de son beau-père à présent retraité.</p><p>Aujourd''hui,un ouvrier et un apprenti ont rejoint l''équipe et il n''est pas rare que nous accueillions un stagiaire, la transmission des savoir-faire étant une valeur chère à nos yeux.</p>'),
(14, 'Nos valeurs', 'assets/images/section1/valeurs.JPG', 'img_Valeurs', '<p>La satisfaction du client par un travail de qualité, des finitions soignées et un chantier propre après notre départ.</p><p>Votre santé et la nôtre par le choix préférentiel d''huiles et peintures <strong>sans solvant</strong>.</p><p>La protection de l''<strong>environnement</strong> par la promotion des bois de chez nous (chêne,hêtre,mélèze...) venant de forêts gérées de manière durable.&nbsp;</p>'),
(24, 'Notre équipe', 'assets/images/section1/François.jpg', 'François', 'François, menuisier et gérant, est parti en 2005 se perfectionner en France après son apprentissage. En 2010, de retour en Belgique, il s''installe comme indépendant avant de créer cette entreprise en 2014.'),
(25, 'Notre équipe', 'assets/images/section1/Renier.jpg', 'Renier', 'Renier est <strong>ébéniste</strong> de formation. Il a également approfondi sa maîtrise du travail du bois en France, en Angleterre et en Russie. Il rejoint notre<strong> menuiserie</strong> en 2017 et lui fait profiter de ses talents et idées ingénieuses.'),
(27, 'Notre équipe', 'assets/images/section1/Jérôme.jpg', 'Jérôme', 'Jérôme, apprenti menuisier, est avec nous depuis 2018. Quand il n''est pas sur chantier avec François, il travaille avec Renier à l''atelier. Il bénéficie de l''expérience de ses aînés et les assiste de manière efficace.'),
(28, 'Notre équipe', 'assets/images/section1/logo-femme.png', 'Fabienne', 'Fabienne, épouse de François, donne un coup de main apprécié du côté administratif: factures, <strong>devis</strong>, site internet, page Facebook....(Tout cela à côté de son boulot d''ingénieur).'),
(29, 'Que proposons nous ?', 'assets/images/section1/maison-9-1(recadre).jpg', 'img_QueProposonsNous', '<p>Nous travaillons le <strong>bois massif</strong> bien sûr, mais aussi les panneaux.Nous fabriquons <strong>sur mesure</strong> <strong>placards</strong>, <strong>portes</strong>, <strong>escaliers</strong>, <strong>tables</strong>, <strong>car-port</strong>...et assurons également la pose de <strong>plancher</strong>, <strong>châssis en bois de bardage</strong>,<strong>terrasse</strong>...</p><p>Nous sommes membre de l''UAP(Union des Artisans du Patrimoine) et travaillons régulièrement pour les Monuments et Sites.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `realisations`
--

CREATE TABLE IF NOT EXISTS `realisations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) DEFAULT NULL,
  `image_path` varchar(150) NOT NULL,
  `alt_image` varchar(255) NOT NULL,
  `text` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `realisations`
--

INSERT INTO `realisations` (`id`, `titre`, `image_path`, `alt_image`, `text`) VALUES
(1, 'Nos réalisations', '', '', ''),
(23, 'Nos réalisations', 'assets/images/section2/rea1.jpg', 'garde-corp', 'Garde-corps et escalier en douglas.'),
(25, 'Nos réalisations', 'assets/images/section2/rea3.jpg', 'Bibliothèque coulissante', 'Bibliothèque coulissante en 3 épaisseurs.'),
(27, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape1.jpg', 'porte_étape1', '<p><span style="background-color: transparent;">1.Le bois brut : chêne premier choix.</span></p>'),
(31, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape2.jpg', 'porte_étape2', '<p>2.On débite, on dégauchit et on rabote.</p>'),
(32, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape3.jpg', 'porte_étape3', '<p>3.Toutes les masses capables.</p>'),
(33, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape4.jpg', 'porte_étape4', '<p>4.Le puzzle prêt à assembler.</p>'),
(34, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape5.jpg', 'porte_étape5', '<p>5.Un détail qui fait la différence.</p>'),
(35, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape6.jpg', 'porte_étape6', '<p>6.L''assemblage en atelier.</p>'),
(36, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape7.jpg', 'porte_étape7', '<p>7.L''ancienne et la nouvelle.</p>'),
(37, 'Une réalisation de A à Z', 'assets/images/section2/porte_etape8.jpg', 'porte_étape8', '<p>8.Porte avec imposte vitrée plus isolante, plus étanche et toujours aussi belle.</p>'),
(38, 'Nos réalisations', 'assets/images/section2/Bardage_Cedre.jpg', 'Bardage Cèdre', 'Bardage en cèdre ajouré.'),
(39, 'Nos réalisations', 'assets/images/section2/20190507_084346.jpg', 'Garde-corps chêne et inox', 'Garde-corps chêne et inox, plancher en chêne.'),
(40, 'Nos réalisations', 'assets/images/section2/Terrasse.jpg', 'terrasse en bois exotique', 'Terrasse en bois exotique.'),
(41, 'Nos réalisations', 'assets/images/section2/Escalier.jpg', 'escaliers en colimaçon', 'Garde-corps en chêne et inox, poteau cintré.'),
(42, 'Nos réalisations', 'assets/images/section2/meuble_et_etagere.jpg', 'meubles peint', 'Meuble peint avec tablette en chêne massif.'),
(43, 'Nos réalisations', 'assets/images/section2/placard_sur_mesure.jpg', 'dressing sur mesure', 'Dressing sur mesure, porte à peindre.'),
(44, 'Nos réalisations', 'assets/images/section2/Table_de_Salon.jpg', 'Table de salon', 'Table de salon en chêne massif et verre.'),
(45, 'Nos réalisations', 'assets/images/section2/IMG-20190506-WA0019.jpg', 'fenêtre jaunes', 'Restauration de châssis à l''identique sur bâtiments classés'),
(46, 'Nos réalisations', 'assets/images/section2/rea2.jpg', 'Terrasse en hauteur', 'Terrasse en hauteur.');

-- --------------------------------------------------------

--
-- Table structure for table `votre_projet`
--

CREATE TABLE IF NOT EXISTS `votre_projet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(150) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `alt_image` varchar(150) DEFAULT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `votre_projet`
--

INSERT INTO `votre_projet` (`id`, `titre`, `image_path`, `alt_image`, `text`) VALUES
(1, 'Votre projet', 'assets/images/section3/19643756391200933674PICT0009.jpg', 'Escalier en noir et blanc', '<p></p><ul><li>Vous avez une idée, un projet, un rêve en bois et vous prenez contact avec nous.</li><li>François passe sur place pour discuter avec vous, comprendre votre souhait et prendre les premières mesures afin de préparer un devis.</li><li>Vous recevez votre devis, nous en discutons, proposons éventuellement des variantes.</li><li>Une fois le projet bien défini et le devis signé, nous planifions les travaux en tenant compte de vos impératifs.</li><li>Vous êtes toujours le bienvenu à l''atelier pour venir voir la fabrication en cours.</li><li>Quand tout est prêt, nous venons faire la pose chez vous, en accordant un grand soin aux finitions ainsi qu''au nettoyage du chantier avant notre départ.</li><li>Un client satisfait est notre meilleure carte de visite.</li></ul><p></p>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
