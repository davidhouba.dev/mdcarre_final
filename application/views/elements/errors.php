<link rel="stylesheet" href="<?= base_url();?>assets/css/admin.css">

<!-- c'est un tableau qui reprend toutes erreurs -->

<?php  if (count($errors) > 0) : ?>
	<div class="error">
		<?php foreach ($errors as $error) : ?>
			<p><?php echo $error ?></p>
		<?php endforeach ?>
	</div>
<?php  endif ?>
