<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/admin.css">
	<title>Espace admin</title>
</head>
<body>
<h1 class="titre_panel">Panel administrateur</h1>
<h2 class="titre_panel">Bonjour François !</h2>

<div id="menu_admin">

	<?php ?>

<ul>
	<fieldset>
		<legend>Gestion du site</legend>
			<li><a href="<?= base_url(). 'editionSection/updateHeader/'?>">En tête du site</a></li>
			<li><a href="<?= base_url(). 'editionSection/updateSection1/'?>">Qui sommes nous ?</a></li>
			<li><a href="<?= base_url(). 'editionSection/updateSection2/'?>">Nos réalisations</a></li>
			<li><a href="<?= base_url(). 'editionSection/updateSection3/'?>">Votre projet</a></li>
			<li><a href="<?= base_url(). 'editionSection/updateContact/'?>">Contactez-nous</a></li>
			<li><a href="<?= base_url(). 'editionSection/updateFooter/'?>">Liens des partenaires</a></li>
	</fieldset>
	<fieldset id="second_field">
		<legend>Gestion administrateur</legend>
			<li><a href="<?= base_url().'GestionAdministrateur/updateAdministrateur'?>">Modifier les identifiants de connexion</a></li>
			<li><a href="<?= base_url(); ?>connection/logout">Déconnexion</a></li>
	</fieldset>
</ul>
</div>
</body>
</html>
