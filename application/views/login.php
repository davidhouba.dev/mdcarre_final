<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url();?>assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url();?>assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url();?>assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= base_url();?>assets/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?= base_url();?>assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<link rel="stylesheet" href="<?= base_url();?>assets/css/reset.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/css/admin.css">

	<title>Page de connexion</title>
</head>
<body>
<h1 class="titre_panel_login">Connexion Administrateur :</h1>

<div id="form">

<form action="" method="post" action="<?= base_url();?>connection/login" id="formulaireDeConnexion">
	<?php
	$this->load->view('elements/errors.php',$errors);
	?>

	<p><label for="email">Email</label></p><p><input type="email" name="email" required ></p>
	<p><label for="password">Mot de passe</label></p><p><input type="password" name="password" required ></p>
	<p><button class="submit" name="submit_admin">Se connecter</button></p>

</form>

</div>

</body>
</html>
