<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta http-equiv="X-UA-Compatible" content="IE=9;IE=8;IE=7;IE=edge" />
	<!--[if lt IE 9]>
	<script src="<?=base_url();?>assets/js/html5shiv.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?= base_url();?>assets/css/reset.css">
	<link  media="screen" rel="stylesheet" href="<?= base_url();?>assets/css/mdCarre.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url();?>assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url();?>assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url();?>assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= base_url();?>assets/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?= base_url();?>assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<title>MdCarre</title>
</head>
<body>

<header id="header">
	<nav class="topnav" id="myTopnav">
		<div id="iconeHamburger">
		<a href="javascript:void(0);" class="icon" onclick="myFunction_Hamburgeur()">
			<i class="fa fa-bars"></i>
		</a>
		</div>
		<ul>

			<img src="<?= base_url(); ?>assets/images/menu/logo-md2.png" alt="logo" >
			<li><i class="fas fa-home"></i><a href="#header">Accueil</a></li>
			<li><i class="fas fa-users"></i><a href="#smoothScroll1"><?= $afficheTitrePrincipaleSection1[0]['titre']?></a></li>
			<li><i class="fas fa-drafting-compass"></i><a href="#smoothScroll2"><?= $afficheTitrePrincipaleSection2[0]['titre']?></a></li>
			<li><i class="fas fa-pencil-alt"></i><a href="#smoothScroll3"><?= $afficheVotreProjet[0]['titre']?></a></li>
			<li id="rotationIcone"><i class="fas fa-phone"></i><a href="#smoothScroll4">Contactez-nous</a></li>

		</ul>
	</nav>
</header>


	<div class="slideshow-container">

		<div class="mySlides slide">
			<img src="<?= base_url(); ?><?= $afficheImageHeader[0]['image_path']?>" alt="<?= $afficheImageHeader[0]['alt_image']?>" style="width:100%">
		</div>

		<div class="mySlides slide">
			<img src="<?= base_url(); ?><?= $afficheImageHeader[1]['image_path']?>" alt="<?= $afficheImageHeader[1]['alt_image']?>" style="width:100%" >
		</div>

		<div class="mySlides slide">
			<img src="<?= base_url(); ?><?= $afficheImageHeader[2]['image_path']?>" alt="<?= $afficheImageHeader[2]['alt_image']?>" style="width:100%">
		</div>

	</div>



	<!-- The dots/circles -->
	<div style="text-align:center;margin: 0.5% 0 0.5% 0">
		<span class="dot"></span>
		<span class="dot"></span>
		<span class="dot"></span>
	</div>



