<footer>
    <script src="<?= base_url(); ?>assets/js/cookiechoices.js"></script><script>document.addEventListener('DOMContentLoaded', function(event){cookieChoices.showCookieConsentBar('Ce site utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l’utilisation des cookies.', 'J’accepte', 'En savoir plus', '<?= base_url(); ?>pages/mentionsLegales');});</script>

	<p>Retrouvez nos partenaires : <a href="<?= $afficheTextFooter[0]['lien']?>" target="_blank"><?= $afficheTextFooter[0]['text']?></a> - <a href="<?= $afficheTextFooter[1]['lien']?>" target="_blank"><?= $afficheTextFooter[1]['text']?></a> - <a href="<?= $afficheTextFooter[2]['lien']?>" target="_blank"><?= $afficheTextFooter[2]['text']?></a></p>
	<p><a href="<?= base_url(); ?>pages/mentionsLegales">Mentions légales</a> - Designed with <3 by Houba David</p>
	<p>Les icônes sont sous licence Creative Commons Attribution 4.0 International license</p>

</footer>

</body>

<script src="<?= base_url();?>assets/js/hamburgeur.js"></script>
<script src="<?= base_url();?>assets/js/smoothScroll.js"></script>
<script src="<?= base_url();?>assets/js/slider.js"></script>
<script src="<?= base_url();?>assets/js/galerieSection2Partie1.js"></script>
<script src="<?= base_url();?>assets/js/galerieSection2Partie2.js"></script>




</html>
