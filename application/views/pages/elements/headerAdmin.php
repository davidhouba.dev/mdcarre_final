<meta http-equiv="X-UA-Compatible" content="IE=9;IE=8;IE=7;IE=edge" />
<!--[if lt IE 9]>
<script src="<?=base_url();?>assets/js/html5shiv.js"></script>
<![endif]-->

<link rel="stylesheet" href="<?= base_url(); ?>assets/css/admin.css">
<link rel="stylesheet" href="<?= base_url();?>assets/css/reset.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/trumbowyg.min.js"></script>
<script src="<?= base_url(); ?>assets/js/trumbowyg.upload.min.js"></script>

<script src="<?= base_url(); ?>assets/js/trumbowyg.colors.min.js"></script>
<script src="<?= base_url(); ?>assets/js/trumbowyg.history.min.js"></script>
<script src="<?= base_url(); ?>assets/js/trumbowyg.table.min.js"></script>
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.colors.min.css">
<link rel="stylesheet" href="<?= base_url(); ?>assets/css/trumbowyg.table.min.css">

<div class="header">
	<p><a  href="<?= base_url() ?>pages/index"><img src="<?= base_url(); ?>assets/images/menu/logo-md2.png" alt=""></a></p>
	<p><button class="submit"><a href="<?= base_url()."connection/logout";?>">Déconnexion</a></button></p>
</div>
