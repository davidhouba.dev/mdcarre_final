<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>



<div class="formulaireModification">
	<?php if(isset($page)):?>
		<h2 class="titre_panel">Edition de la section "Nos réalisations" </h2>

		<form id="form" method="post">
			<?php if($page->id): ?>
				<input name="id" value="<?=$page->id?>" hidden>
			<?php endif;?>
			<?php if($page->titre): ?>
				<input name="oldnom" value="<?=$page->titre?>" hidden>
			<?php endif;?>

			<div class="fondTitre">
				<p><label for="titre<?=$page->id?>" >Titre de la section :</label></p>
				<p><input id="titre<?=$page->id?>" value="<?=$page->titre?>" name="titre<?=$page->id?>" type="text"></p>
			</div>

			<?php foreach ($premiereGallerie as $key) : ?>

				<input name="idLigne" value="<?=$key->id?>" hidden>

				<div class="fondInput">
					<p><label for="image<?=$key->id?>">Selectionnez l'image à uploader:</label></p>
					<p><input id="image<?=$key->id?>" name="image<?=$key->id?>" type="file"  accept="image/*"></p>
				</div>

				<div class="fondInput">
					<p><label for="alt_image<?=$key->id?>">Alt (C'est le texte qui s'affiche en cas d'erreur de chargement de l'image) :</label></p>
					<p><input id="alt<?=$key->id?>" value="<?=$key->alt_image?>" name="alt_image<?=$key->id?>" type="text"></p>
				</div>

				<div class="fondInput">
					<label for="text<?=$key->id?>">Contenu :</label>
					<textarea id="text<?=$key->id?>" name="text<?=$key->id?>" class="trumbowyg">
                </textarea>
				</div>

				<p id="delete">
					<button class="submit" type="button" data-id="<?=$key->id?>" name="" data-target="#delete" >Supprimer</button>
				</p>


			<?php endforeach; ?>

			<div>
				<a href="<?= base_url().'Connection/login'?>" class="submit">Retour</a>
				<button class="submit" type="submit" id="submit" name="submit">Envoyer</button>
			</div>

			<div class="styleError" name="successUpdatePage" style="display: none;">
				La page a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
			</div>
			<div class="styleError" name="errorUpdatePage" style="display: none;">
				La page n'a pas pu être mise à jour. Si le problème persiste, contactez un administrateur système.
			</div>

		</form>


	<?php endif; ?>
	<h2 class="titre_panel">Ajouter un élément </h2>

	<form id="form2" method="post">

		<div class="fondInput">
			<p><label for="imageAjoute">Selectionnez l'image à uploader:</label></p>
			<p><input id="imageAjoute" name="imageAjoute" type="file"  accept="image/*" required></p>
		</div>

		<div class="fondInput">
			<p><label for="alt_imageAjoute">Alt (C'est le texte qui s'affiche en cas d'erreur de chargement de l'image) :</label></p>
			<p><input id="altAjoute" value="" name="alt_imageAjoute" type="text" required></p>
		</div>

		<div class="fondInput">
			<label for="textAjoute">Contenu :</label>
			<textarea id="textAjoute" name="textAjoute" class="trumbowyg">
                </textarea>
		</div>

		<div>
			<button class="submit" type="submit" id="submitAjouter" name="submitAjouter">Envoyer</button>
		</div>

		<div class="styleError" name="successUpdatePage2" style="display: none;">
			La page a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
		</div>
		<div class="styleError" name="errorUpdatePage2" style="display: none;">
			La page n'a pas pu être mise à jour. Si le problème persiste, contactez un administrateur système.
		</div>

		<div class="styleError" name="successUpdatePage4" style="display: none;">
			La page a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
		</div>
		<div class="styleError" name="errorUpdatePage4" style="display: none;">
			La page n'a pas pu être mise à jour. Si le problème persiste, contactez un administrateur système.
		</div>

	</form>

	<!-- //////////---------Début de la sous section------\\\\\\\\\\ -->

	<div class="formulaireModification">
		<?php if(isset($page)):?>
			<h2 class="titre_panel">Edition de la sous-section "Une réalisation de A à Z" </h2>

			<form id="form3" method="post">
				<?php if($page->id): ?>
					<input name="id" value="<?=$page->id?>" hidden>
				<?php endif;?>

				<?php foreach ($sousSection as $key) : ?>

					<input name="idLigne" value="<?=$key->id?>" hidden>

					<div class="fondInput">
						<p><label for="imageUpdate<?=$key->id?>">Selectionnez l'image à uploader:</label></p>
						<p><input id="image<?=$key->id?>" name="imageUpdate<?=$key->id?>" type="file"  accept="image/*"></p>
					</div>

					<div class="fondInput">
						<p><label for="alt_image<?=$key->id?>">Alt (C'est le texte qui s'affiche en cas d'erreur de chargement de l'image) :</label></p>
						<p><input id="alt<?=$key->id?>" value="<?=$key->alt_image?>" name="alt_image<?=$key->id?>" type="text"></p>
					</div>

					<div class="fondInput">
						<label for="text<?=$key->id?>">Contenu :</label>
						<textarea id="text<?=$key->id?>" name="text<?=$key->id?>" class="trumbowyg">
                </textarea>

					</div>
					<p id="delete">
						<button class="submit" type="button" data-id="<?=$key->id?>" name="" data-target="#delete" >Supprimer</button>
					</p>

					<div class="styleError" name="successUpdatePage4" style="display: none;">
						La db a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
					</div>
					<div class="styleError" name="errorUpdatePage4" style="display: none;">
						Un problème est survenu. Si le problème persiste, contactez un administrateur système.
					</div>

				<?php endforeach; ?>

				<div>
					<a href="<?= base_url().'Connection/login'?>" class="submit">Retour</a>
					<button class="submit" type="submit" id="submit" name="submit">Envoyer</button>
				</div>

				<div class="styleError" name="successUpdatePage3" style="display: none;">
					La db a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
				</div>
				<div class="styleError" name="errorUpdatePage3" style="display: none;">
					Un problème est survenu. Si le problème persiste, contactez un administrateur système.
				</div>

			</form>


		<?php endif; ?>
		<h2 class="titre_panel">Ajouter un élément </h2>

		<form id="form4" method="post">

			<div class="fondInput">
				<p><label for="imageAjoute">Selectionnez l'image à uploader:</label></p>
				<p><input id="imageAjoute" name="imageAjoute" type="file"  accept="image/*" required></p>
			</div>

			<div class="fondInput">
				<p><label for="alt_imageAjoute">Alt (C'est le texte qui s'affiche en cas d'erreur de chargement de l'image) :</label></p>
				<p><input id="altAjoute" value="" name="alt_imageAjoute" type="text" required></p>
			</div>

			<div class="fondInput">
				<label for="textAjoute">Contenu :</label>
				<textarea id="textAjoute" name="textAjoute" class="trumbowyg">
                </textarea>
			</div>

			<div>
				<button class="submit" type="submit" id="submitAjouter" name="submitAjouter">Envoyer</button>
			</div>

			<div class="styleError" name="successUpdatePage4" style="display: none;">
				La db a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
			</div>
			<div class="styleError" name="errorUpdatePage4" style="display: none;">
				Un problème est survenu. Si le problème persiste, contactez un administrateur système.
			</div>

		</form>
</div>
</div>

	<!-- //////////---------fin de la sous section------\\\\\\\\\\ -->

	<script>
		$.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
		var config = {
			lang: 'fr',
			btnsDef: {
				// Create a new dropdown
				image: {
					dropdown: ['insertImage', 'upload'],
					ico: 'insertImage'
				}
			},
			// Redefine the button pane
			btns: [
				['historyUndo','historyRedo'],
				['formatting'],
				['strong', 'em', 'underline'],
				['superscript', 'subscript'],
				['link'],
				['image'], // Our fresh created dropdown
				/*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
				['unorderedList', 'orderedList'],
				['horizontalRule'],
				['removeformat'],
				['foreColor', 'backColor'],
				['fullscreen'],
				['table']
			],
			plugins: {
				// Add imagur parameters to upload plugin for demo purposes
				upload: {
					serverPath: "<?= base_url();?>index.php/upload/image",
					urlPropertyName: 'url'
				},
				resizimg : {
					minSize: 20,
					step: 1,
				},
				table: {
					rows:7,
					columns:7,
					styler:'table',
				}
			}
		};
		$('.trumbowyg').trumbowyg(config);

		$("#titre").val(`<?=$page->titre?>`);

		<?php foreach ($premiereGallerie as $premiereGallerie):?>
		$("#text<?=$premiereGallerie->id?>").trumbowyg('html', '<?= (addslashes($premiereGallerie->text));?>');
		<?php endforeach; ?>

		<?php foreach ($sousSection as $sousSection):?>
		$("#text<?=$sousSection->id?>").trumbowyg('html', '<?= (addslashes($sousSection->text));?>');
		<?php endforeach; ?>


		$('#form').submit(function(e){
			e.preventDefault();
			var formData = new FormData(this);
			$.ajax({
				method: 'POST',
				url: '<?=base_url().'editionSection/updateSection2'?>',
				data: formData,
				processData: false,
				contentType: false,
				error: function(){
					$('div[name=errorUpdatePage]').fadeIn(400, function(){
						setTimeout(function(){
							$('div[name=errorUpdatePage]').fadeOut();
						}, 3000)
					})
				},
				success: function(){
					$('div[name=successUpdatePage]').fadeIn(400, function(){
						setTimeout(function(){
							window.location = '<?= base_url()."Connection/login"?>';
						}, 3000)
					});
				}
			});
		});

		$('#form2').submit(function(e){
			e.preventDefault();
			var formData = new FormData(this);
			$.ajax({
				method: 'POST',
				url: '<?=base_url().'editionSection/ajouteSection2'?>',
				data: formData,
				processData: false,
				contentType: false,
				error: function(){
					$('div[name=errorUpdatePage2]').fadeIn(400, function(){
						setTimeout(function(){
							$('div[name=errorUpdatePage2]').fadeOut();
						}, 3000)
					})
				},
				success: function(){
					$('div[name=successUpdatePage2]').fadeIn(400, function(){
						setTimeout(function(){
							window.location = '<?= base_url()."Connection/login"?>';
						}, 3000)
					});
				}
			});
		});

		$('#form3').submit(function(e){
			e.preventDefault();
			var formData = new FormData(this);
			$.ajax({
				method: 'POST',
				url: '<?=base_url().'editionSection/updateSection2'?>',
				data: formData,
				processData: false,
				contentType: false,
				error: function(){
					$('div[name=errorUpdatePage3]').fadeIn(400, function(){
						setTimeout(function(){
							$('div[name=errorUpdatePage3]').fadeOut();
						}, 3000)
					})
				},
				success: function(){
					$('div[name=successUpdatePage3]').fadeIn(400, function(){
						setTimeout(function(){
							window.location = '<?= base_url()."Connection/login"?>';
						}, 3000)
					});
				}
			});
		});

		$('#form4').submit(function(e){
			e.preventDefault();
			var formData = new FormData(this);
			$.ajax({
				method: 'POST',
				url: '<?=base_url().'editionSection/ajouteSousSection2'?>',
				data: formData,
				processData: false,
				contentType: false,
				error: function(){
					$('div[name=errorUpdatePage3]').fadeIn(400, function(){
						setTimeout(function(){
							$('div[name=errorUpdatePage3]').fadeOut();
						}, 3000)
					})
				},
				success: function(){
					$('div[name=successUpdatePage3]').fadeIn(400, function(){
						setTimeout(function(){
							window.location = '<?= base_url()."Connection/login"?>';
						}, 3000)
					});
				}
			});
		});

		$('#delete button').click(function(e){
			var button = e.target; // récupère les propriétés de l'html liées au bouton sur lequel on clique
			var id = $(button).attr('data-id'); //on récupère l'attribut 'data-id' récupéré avec la variable 'button'

			$.ajax({
				method: 'POST',
				url: '<?=base_url().'editionSection/deleteSection2'?>',
				data: {id:id},
				error: function(){
					$('div[name=errorUpdatePage4]').fadeIn(400, function(){
						setTimeout(function(){
							$('div[name=errorUpdatePage4]').fadeOut();
						}, 3000)
					})
				},
				success: function(){
					$('div[name=successUpdatePage4]').fadeIn(400, function(){
						setTimeout(function(){
							window.location = '<?= base_url() . "Connection/login"?>';
						}, 3000)
					});
				}
			});
		});


</script>
