<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link  media="screen" rel="stylesheet" href="<?= base_url();?>assets/css/mdCarre.css">
	<title>Document</title>
</head>
<body>
<div id="mentionsLegales">

	<h2>Droit d'auteur</h2>
	<p>Le site https://mdcarre-test.000webhostapp.com/ constitue une création protégée par le droit d'auteur. Les textes, photos et autres éléments de mon site sont protégés par le droit d'auteur. Toute copie, adaptation, traduction, arrangement, communication au public, location et autre exploitation, modification de tout ou partie de ce site sous quelle que forme et par quel que moyen que ce soit, électronique, mécanique ou autre, réalisée dans un but lucratif ou dans un cadre privé, est strictement interdit sans mon autorisation préalable .Toute infraction à ces droits entrainera des poursuites civiles ou pénales.</p>

	<h2>Marques et noms commerciaux</h2>
	<p>Les dénominations, logos et autres signes utilisés sur mon site sont des marques et/ou noms commerciaux légalement protégés. Tout usage de ceux-ci ou de signes ressemblants est strictement interdit sans un accord préalable et écrit.</p>

	<h2>Responsabilité quant au contenu</h2>
	<p>J'apporte le plus grand soin à la création et à la mise à jour de ce site mais je ne peux toutefois pas garantir l'exactitude de l'information qui s'y trouve. Les informations contenues dans ce site pourront faire l'objet de modifications sans préavis. Les informations données sur ce site ne sauraient engager ma responsabilité, je ne pourrais être tenu pour responsable de toute omission, erreur ou lacune qui aurait pu se glisser dans ses pages ainsi que des conséquences, quelles qu'elles soient, pouvant résulter de l'utilisation des informations et indications fournies.</p>

	<h2>Coordonnées</h2>
	<p>Vous pouvez me contacter par Email à info@mdcarre.be</p>
		<p>Rue de Gembloux 500/10
			5002 Saint-Servais</p>
		<p>0494/28 43 17</p>
	<h2>Hébergeur</h2>
	<p>One</p>
		<p>57 Rue d’Amsterdam
			Paris 75008
			France</p>
		<p>www.one.com</p>

	<h2>Cookies</h2>
	<h3>Définition d'un cookie</h3>
	<p>Un cookie est un fichier texte déposé sur le disque dur de votre ordinateur, de votre appareil mobile ou de votre tablette lors de la visite d'un site ou de la consultation d'une publicité. Il a pour but de collecter des informations relatives à votre navigation et de vous adresser des services adaptés à votre terminal.
		Le cookie contient un code unique permettant de reconnaître votre navigateur lors de votre visite sur le site web ou lors de futures visites répétées. Les cookies peuvent être placés par le serveur du site web que vous visitez ou par des partenaires avec lesquels ce site web collabore. Le serveur d’un site web peut uniquement lire les cookies qu’il a lui-même placés et n’a accès à aucune autre information se trouvant sur votre ordinateur ou sur votre appareil mobile.
		Les cookies assurent une interaction généralement plus aisée et plus rapide entre le visiteur et le site web. En effet, ils mémorisent vos préférences (la langue choisie ou un format de lecture par exemple) et vous permettent ainsi d’accélérer vos accès ultérieurs au site et de faciliter vos visites.
		De plus, ils vous aident à naviguer entre les différentes parties du site web. Les cookies peuvent également être utilisés pour rendre le contenu d’un site web ou la publicité présente plus adaptés aux choix, goûts personnels et aux besoins du visiteur.
		Les informations ainsi récoltées sont anonymes et ne permettent pas votre identification en tant que personne. En effet, les informations liées aux cookies ne peuvent pas être associées à un nom et/ou prénom parce qu’elles ne contiennent pas de données à caractère personnel.
		Les cookies sont gérés par votre navigateur internet. L’utilisation des cookies nécessite votre consentement préalable et explicite. Vous pourrez toujours revenir ultérieurement sur celui-ci et refuser ces cookies et/ou les supprimer à tout moment, en modifiant les paramètres de votre navigateur.</p>


	<h2>Autoriser ou bloquer les cookies ?</h2>
	<p>La plupart des navigateurs internet sont automatiquement configurés pour accepter les cookies. Cependant, vous pouvez configurer votre navigateur afin d’accepter ou de bloquer les cookies.
		Je ne peux cependant vous garantir l’accès à tous les services de mon site internet en cas de refus d’enregistrement de cookies.</p>


	<a class="myButton2" href="<?= base_url(); ?>">Retourner sur le site</a>
</div>

</body>
</html>





