<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="formulaireModification">
	<?php if(isset($page)):?>
		<h2 class="titre_panel">Edition des partenaires </h2>

		<form id="formFooter" method="post">
			<?php if($page->id): ?>
				<input name="id" value="<?=$page->id?>" hidden>
			<?php endif;?>
			<?php if($page->text): ?>
				<input name="oldnom" value="<?=$page->text?>" hidden>
			<?php endif;?>

			<?php foreach ($footer as $key) : ?>

				<input name="idLigne" value="<?=$key->id?>" hidden>

				<div class="fondInput">
					<p><label for="text<?=$key->id?>"><?= $key->titre?> :</label></p>
					<p><input id="text<?=$key->id?>" value="<?=$key->text?>" name="text<?=$key->id?>" type="text"></p>

					<p><input id="lien<?=$key->id?>" value="<?=$key->lien?>" name="lien<?=$key->id?>" type="text"></p>
				</div>

			<?php endforeach; ?>

			<div>
				<a href="<?= base_url().'Connection/login'?>" class="submit">Retour</a>
				<button class="submit" type="submit" id="submit" name="submit">Envoyer</button>
			</div>

			<div class="styleError" name="successUpdatePage" style="display: none;">
				La page a été mise à jour ! Vous serez redirigé automatiquement à la liste des pages dans 3 secondes.
			</div>
			<div class="styleError" name="errorUpdatePage" style="display: none;">
				La page n'a pas pu être mise à jour. Si le problème persiste, contactez un administrateur système.
			</div>

		</form>


	<?php endif; ?>
</div>



<script>
	$.trumbowyg.svgPath = '<?= base_url(); ?>assets/css/icons.svg';
	var config = {
		lang: 'fr',
		btnsDef: {
			// Create a new dropdown
			image: {
				dropdown: ['insertImage', 'upload'],
				ico: 'insertImage'
			}
		},
		// Redefine the button pane
		btns: [
			['historyUndo','historyRedo'],
			['formatting'],
			['strong', 'em', 'underline'],
			['superscript', 'subscript'],
			['link'],
			['image'], // Our fresh created dropdown
			/*  */           ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			['unorderedList', 'orderedList'],
			['horizontalRule'],
			['removeformat'],
			['foreColor', 'backColor'],
			['fullscreen'],
			['table']
		],
		plugins: {
			// Add imagur parameters to upload plugin for demo purposes
			upload: {
				serverPath: "<?= base_url();?>index.php/upload/image",
				urlPropertyName: 'url'
			},
			resizimg : {
				minSize: 20,
				step: 1,
			},
			table: {
				rows:7,
				columns:7,
				styler:'table',
			}
		}
	};
	$('.trumbowyg').trumbowyg(config);



	// Affiche dans les inputs le contenu correspondant de la base de donnée
	<?php foreach ($footer as $footer):?>
	$("#text<?=$footer->id?>").trumbowyg('html', '<?php echo (addslashes($footer->text));?>');
	<?php endforeach; ?>

	$('#formFooter').submit(function(e){
		e.preventDefault();
		var formData = new FormData(this);
		$.ajax({
			method: 'POST',
			url: '<?=base_url().'editionSection/updateFooter'?>',
			data: formData,
			processData: false,
			contentType: false,
			error: function(){
				$('div[name=errorUpdatePage]').fadeIn(400, function(){
					setTimeout(function(){
						$('div[name=errorUpdatePage]').fadeOut();
					}, 3000)
				})
			},
			success: function(){
				$('div[name=successUpdatePage]').fadeIn(400, function(){
					setTimeout(function(){
						window.location ='<?= base_url()."Connection/login"?>';
					}, 3000)
				});
			}
		});
	});
</script>
