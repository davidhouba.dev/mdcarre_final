
<div id="wrapper">
<div id="section1">


<h1 id="smoothScroll1"><?= $afficheTitrePrincipaleSection1[0]['titre']?></h1>


	<div id="illustrions_presentation">

		<ul>
			<?php foreach ($affichePremierePartieSection1 as $affichePremierePartieSection1):?>
			<li>
				<figure>
					<img src="<?= base_url();?><?= $affichePremierePartieSection1['image_path']?>" alt="<?= $affichePremierePartieSection1['alt_image']?>">
					<figcaption class="couleur_fond">
						<h3><?= $affichePremierePartieSection1['titre']?></h3><?= $affichePremierePartieSection1['text']?>
					</figcaption>
				</figure>
			</li>
			<?php endforeach;?>


		</ul>
		
	</div>

	<h2>Notre équipe </h2>

	<div id="equipe">

		<ul>
			<?php foreach ($afficheDeuximePartieSection1 as $afficheDeuximePartieSection1):?>
			<li>
				<figure>
					<img src="<?= base_url();?><?= $afficheDeuximePartieSection1['image_path']?>" alt="<?= $afficheDeuximePartieSection1['alt_image']?>">
					<figcaption class="couleur_fond">
						<p><?= $afficheDeuximePartieSection1['text']?></p>
					</figcaption>
				</figure>
			</li>
			<?php endforeach;?>

		</ul>
	</div>

</div>

	<div id="section2">

<h2 id="smoothScroll2"><?= $afficheTitrePrincipaleSection2[0]['titre']?></h2>
		<div style="text-align:center">
			<p>Cliquez sur les images pour les agrandir:</p>
		</div>

		<div id="autres_realisations">

			<?php for ($i=0;$i < $countModal ;$i++ ):?>
				<div class="container1">
					<img src="<?= base_url();?><?= $affichePremierePartieSection2[$i]['image_path']?>" alt="<?= $affichePremierePartieSection2[$i]['alt_image']?>" onclick="openModal();slideActif(<?=$i+1?>)" class="hover-shadow">
				</div>
			<?php endfor;?>

			<!-- The Modal/Lightbox -->
			<div id="myModal" class="modal">
				<span class="close cursor" onclick="closeModal()">&times;</span>
				<div class="modal-content-autreRealisation">

					<?php for($i=0;$i< $countModal; $i++):?>
						<div class="monSlider">
							<img src="<?= base_url();?><?= $affichePremierePartieSection2[$i]['image_path']?>" alt="<?= $affichePremierePartieSection2[$i]['alt_image']?>" style="width:100%">
							<figcaption><?= $affichePremierePartieSection2[$i]['text']?></figcaption>
						</div>
					<?php endfor;?>

					<!-- Contrôle boutons suivant/précédant -->
					<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
					<a class="next" onclick="plusSlides(1)">&#10095;</a>


					<div id="aperçusGalerie2">
						<!-- contrôle de l'aperçus des images -->
						<?php for ($i=0;$i<$countModal;$i++):?>
							<div class="apercuGalerie">
								<img class="galerie" src="<?= base_url();?><?= $affichePremierePartieSection2[$i]['image_path']?>" alt="<?= $affichePremierePartieSection2[$i]['alt_image']?>" onclick="slideActif(<?=$i+1?>)">
							</div>
						<?php endfor;?>

					</div>
				</div>
			</div>
		</div>


	<h2>Une réalisation de A à Z</h2>

	<div style="text-align:center">
		<p>Cliquez sur les images pour les agrandir:</p>
	</div>

	<div class="wrapper_galerie">

		<?php for ($y=0;$y < $countModal2 ;$y++ ):?>
		<div class="colonne-galerie">
			<img src="<?= base_url();?><?= $afficheDeuxiemePartieSection2[$y]['image_path']?>" alt="<?= $afficheDeuxiemePartieSection2[$y]['alt_image']?>" onclick="openModal2();slideActif2(<?=$y+1?>)" class="hover-shadow">
		</div>
		<?php endfor;?>

		<div id="myModal2" class="modal">
			<span class="close2 cursor" onclick="closeModal2()">&times;</span>
			<div class="modal-content-galerie">

				<?php for($y=0;$y< $countModal2; $y++):?>
				<div class="monSlider2">
					<img src="<?= base_url();?><?= $afficheDeuxiemePartieSection2[$y]['image_path']?>" alt="<?= $afficheDeuxiemePartieSection2[$y]['alt_image']?>" style="width:100%">
					<figcaption><?= $afficheDeuxiemePartieSection2[$y]['text']?></figcaption>
				</div>
			<?php endfor;?>


				<a class="prev" onclick="plusSlides2(-1)">&#10094;</a>
				<a class="next" onclick="plusSlides2(1)">&#10095;</a>


				<div id="aperçusGalerie2">

					<?php for ($y=0;$y<$countModal2;$y++):?>
					<div class="apercuGalerie">
						<img class="galerie2" src="<?= base_url();?><?= $afficheDeuxiemePartieSection2[$y]['image_path']?>" alt="<?= $afficheDeuxiemePartieSection2[$y]['alt_image']?>" onclick="slideActif2(<?=$y+1?>)">
					</div>
					<?php endfor;?>

				</div>
			</div>
		</div>
	</div>
	</div>


	<div id="section3">

		<h2 id="smoothScroll3"><?= $afficheVotreProjet[0]['titre']?></h2>

		<div id="wrapper_section3">

		<div id="img_section3">
			<img src="<?= base_url();?><?= $afficheVotreProjet[0]['image_path']?>" alt="<?= $afficheVotreProjet[0]['alt_image']?>" style="width: 100%">
		</div>

		<div id="nous_realisons_vos_projets">
			<?= $afficheVotreProjet[0]['text']?>
		</div>
		</div>

	</div>

	<div id="section4">
		<h2 id="smoothScroll4">Contactez-nous</h2>

		<div id="wrapper_section4">

		<div id="nos_coordonnees">
			<h3>Nos coordonnées  </h3>

			<p><i class="fas fa-phone-square"></i><?= $afficheTextContact[0]['text']?></p>
			<p><i class="fas fa-envelope"></i><?= $afficheTextContact[1]['text']?></p>
			<p><i class="fas fa-map-marker-alt"></i><?= $afficheTextContact[2]['text']?></p>
			<p><i class="far fa-calendar-alt"></i><?= $afficheTextContact[3]['text']?></p>

			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4136.002762034715!2d4.835033772826553!3d50.48084802106961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c19b81178e5f77%3A0x2ac3c9144dc74ce0!2sRue+de+Gembloux+500%2F100%2C+5002+Saint-Servais+(Namur)!5e1!3m2!1sfr!2sbe!4v1554104951369!5m2!1sfr!2sbe"  height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

			<div id="par_mail">
				<h3>Par mail  </h3>

					<form id="FormContact" action="" method="post">

						<label for="prenom">Prénom</label>
						<input type="text" id="prenom" name="prenom" value="<?php echo !empty($postData['prenom'])?$postData['prenom']:''; ?>" placeholder="Prénom" required>

						<label for="nom">Nom</label>
						<input type="text" id="nom" name="nom" placeholder="Nom" value="<?php echo !empty($postData['nom'])?$postData['nom']:''; ?>" required>

						<label for="email">Email</label>
						<input type="email" id="mail" name="email" value="<?php echo !empty($postData['email'])?$postData['email']:''; ?>" placeholder="Email" required>

						<label for="tel">Téléphone</label>
						<input type="tel" id="tel" name="tel" value="<?php echo !empty($postData['tel'])?$postData['tel']:''; ?>" placeholder="Téléphone">


						<label for="sujet">Sujet</label>
						<textarea id="sujet" name="sujet" value='<?php echo !empty($postData['sujet'])?$postData['sujet']:''; ?>' placeholder="Tapez votre texte ici.." style="height:200px"></textarea>

						<input type="submit" name='contactSubmit' value="Envoyer">


					</form>
			</div>

			<div id="les_dernieres_news">

				<h3>Les dernières infos </h3>


	<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmenuiseriemdcarre%2F&tabs=timeline&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=255569757821550"  height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>


			</div>
		</div>
	</div>
</div>



