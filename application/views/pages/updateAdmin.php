<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div>

	<?php if(isset($administrateur)): ?>
		<form id="form" >
			<p><input id="id" name="id" value="<?=$administrateur->id?>" hidden></p>
			<p><label for="email">Email :</label></p>
			<p><input type="email" id="email" name="email" value="<?= $administrateur->email ?>"  required></p>
			<p><label for="oldpassword" >Ancien mot de passe:</label></p>
			<p><input type="password" name="oldpassword" id="oldpassword" required></p>
			<p><label for="newpassword" >Nouveau mot de passe:</label></p>
			<p><input type="password" id="newpassword" name="newpassword"></p>
			<p><a class="submit" href="<?= base_url(); ?>index.php/Connection/login">Retour</a> <input type="submit" class="submit" value="Enregistrer les changements"></p>

			<div role="alert" name="successUpdateUser" style="display: none;">
				Les identifiants ont été mis à jour ! Vous serez redirigé automatiquement au panel administrateur dans 3 secondes.
			</div>
			<div role="alert" name="errorUpdateUser" style="display: none;">
				Les identifiants n'ont pas pu être mis à jour. Peut-être que les identifiants donnés sont mauvais ? Si ce n'est
				pas le cas, veuillez contacter un administrateur système.
			</div>
</div>
		</form>

	<?php endif; ?>


<?php

?>

<script>
	$('#form').submit(function(e){
		e.preventDefault();
		var formData = new FormData(this);
		$.ajax({
			method: 'POST',
			url: '<?=base_url().'GestionAdministrateur/updateAdministrateur'?>',
			data: formData,
			processData: false,
			contentType: false,
			error: function(){
				$('div[name=errorUpdateUser]').fadeIn(400, function(){
					setTimeout(function(){
						$('div[name=errorUpdateUser]').fadeOut();
					}, 3000)
				})
			},
			success: function(){
				$('div[name=successUpdateUser]').fadeIn(400, function(){
					setTimeout(function(){
						window.location = '<?= base_url()."connection/login"?>';
					}, 3000)
				});
			}
		});
	});

</script>
