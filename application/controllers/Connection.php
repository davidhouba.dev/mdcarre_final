<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Connection extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ConnectionModel');
	}

	public function login()
	{
		session_start();
		$errors = array();

		//on vérifie si l'admin s'est déjà connecté avant
		if (isset($_SESSION['connected'])) {
			$this->load->view('espaceAdmin');
		} else {
			//On récupère les identifiants au moment de cliquer sur le bouton de connection
			if (isset($_POST['submit_admin'])) {

				$password = $_POST['password'];
				$email = $_POST['email'];

				// vérification si email ou password est vide
				if (empty($email)) {
					array_push($errors, "Veuillez entrer votre email");
				}
				if (empty($password)) {
					array_push($errors, "Votre mot de passe est requis");
				}
				if (count($errors) == 0) {
					if ($this->ConnectionModel->logAdmin($email, $password)) {
						//permet de savoir si l'utilisateur aura déjà effectué une connexion
						$_SESSION['connected'] = true;
						$this->load->view('espaceAdmin');
					} else {
						array_push($errors, "L'email ou le mot de passe n'est pas correct");
						$data = array('errors' => $errors);
						$this->load->view('login', $data);
					}
				}
			} else {
				$data = array('errors' => $errors);
				$this->load->view('login', $data);
			}
		}
	}


	//Permet de ce déconnecter en détruisant la session
	public function logout(){
		session_start();
		session_destroy();
		if(isset($_COOKIE[session_name()])):
			setcookie(session_name(), '', time()-7000000, '/');
		endif;
		if(isset($_COOKIE['submit_admin'])):
			setcookie('login_user', '', time()-7000000, '/');
		endif;
		redirect('/');
	}
}
