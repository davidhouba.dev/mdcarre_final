<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EditionSection extends CI_Controller
{

	public function __construct()
	{
		//on charge les différents models
		parent::__construct();
		$this->load->model('Section1Model');
		$this->load->model('Section2Model');
		$this->load->model('Section3Model');
		$this->load->model('HeaderModel');
		$this->load->model('ContactModel');
		$this->load->model('GetSection1');
		$this->load->model('GetSection3');
		$this->load->model('FooterModel');

	}
//-----début de Update du header-----\\
public function updateHeader(){
		session_start(); // Sert à initialiser la session
		if(isset($_SESSION['connected'])){ // Sert à vérifier si l'utilisateur est connecté à la session
			if(isset($_POST['id'])) { //Vérifie si le formulaire a bien été envoyé
				$tmp = new stdClass();
				$allDb = $this->HeaderModel->GetHeader(); // on récupère le contenu de la table header

				// Je fais une boucle sur la table header pour récupérer tout les éléments
				foreach ($allDb as $key) {

					$tmp->alt_image = $_POST['alt_image'.$key->id]; // on récupère le alt de l'image

					if (isset($_FILES['image'.$key->id]) && $_FILES['image'.$key->id]['size'] > 0) { // 'image' correspond au nom du fichier uploader par le formulaire
						$upload_dir = "assets/images/header";
						if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
						}
						$filepath = $upload_dir . '/' . $_FILES['image'.$key->id]['name'];
						if (move_uploaded_file($_FILES['image'.$key->id]["tmp_name"], $filepath)) {
							$tmp->image_path = $filepath;
						}
					} else {
						$old_nom = $this->HeaderModel->getImgHeader($key->id); // on récupère le chemin brut de l'image avant qu'il ne soit modifié on exécute la requete SQL
						$imgPath = $old_nom[0]->image_path; // on récupère la position du chemin de l'image dans le tableau qui est envoyé par la fonction d'update
						$tmp->image_path = $imgPath; // on stock la variable dans une variable temporaire ( stdClass) pour l'envoyer au model
					}

					$this->HeaderModel->updateImgHeader($tmp, $key->id);
					http_response_code(200);
				}
			}else{
				// on envoie les données à la vue
				$tmp = $this->HeaderModel->GetHeader();
				$data['imageHeader']=$tmp;
				$data['page']=$tmp[0];
				$data['pages'] = 'pages/updateHeader';
				$this->load->view("templates/admin", $data);
			}
		}
}

//-----fin de Update du header-----\\

//-----début de Update de la section "Qui somme nous"-----\\

	public function updateSection1()
	{
		session_start();
		if (isset($_SESSION['connected'])) {

			if(isset($_POST['id'])) {
				$id = $_POST['id']; // on récupère l'id du formulaire
				$tmp = new stdClass();
				$tmp6 = new stdClass();
				$tmp4 = $this->Section1Model->GetSection1();
				$titreSection=$tmp4[0]->titre;
				$allDb = $this->Section1Model->getSection1_exclu_titre($titreSection);
				$allNotreEquipe=$this->Section1Model->getNotreEquipe();

				if (isset($_POST['titre1']) && strlen($_POST['titre1']) > 0){
					$tmp->titre = $_POST['titre1'];
				}else{
					$titreSection=$tmp4[0]->titre;
					$tmp->titre = $titreSection;
				}
				$this->Section1Model->updateTitreSection1($tmp, $id);


				//je boucle sur la première partie en exluant le titre principale et la deuxième partie de la section " notre équipe"
				foreach ($allDb as $key) {

					if(isset($_POST['titre'.$key->id]) && strlen($_POST['titre'.$key->id] ) > 0){
						$tmp->sousTitre = $_POST['titre'.$key->id];

					}else{
						$old_nom=$this->Section1Model->getSousTitreSection1($key->id);
						$titrePath=$old_nom[0]->titre;
						$tmp->sousTitre = $titrePath;
					}

					if(isset($_POST['alt_image'.$key->id]) && strlen($_POST['alt_image'.$key->id] ) > 0){
						$tmp->alt_image = $_POST['alt_image'.$key->id];

					}else{
						$old_nom=$this->Section1Model->getAltImageSection1($key->id);
						$altImagePath=$old_nom[0]->alt_image;
						$tmp->alt_image = $altImagePath;
					}

					if(isset($_POST['text'.$key->id]) && strlen($_POST['text'.$key->id] ) > 0){
						$tmp->text = $_POST['text'.$key->id];

					}else{
						$old_nom=$this->Section1Model->getTextSection1($key->id);
						$textPath=$old_nom[0]->text;
						$tmp->text = $textPath;
					}

					if (isset($_FILES['image'.$key->id]) && $_FILES['image'.$key->id]['size'] > 0) {
						$upload_dir = "assets/images/section1";
						if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
						}
						$filepath = $upload_dir . '/' . $_FILES['image'.$key->id]['name'];
						if (move_uploaded_file($_FILES['image'.$key->id]["tmp_name"], $filepath)) {
							$tmp->image_path = $filepath;
						}
					} else {
						$old_nom = $this->Section1Model->getImgSection1($key->id);
						$imgPath = $old_nom[0]->image_path;
						$tmp->image_path = $imgPath;
					}
					$this->Section1Model->updatePartie1Section1($tmp, $key->id);
					http_response_code(200);
				}

				//je boucle sur la deuxième partie en préçisant qu'il s'agit des éléments dont le titre est égal à "Notre équipe"
				foreach ($allNotreEquipe as $key){
					if(isset($_POST['alt_image'.$key->id]) && strlen($_POST['alt_image'.$key->id] ) > 0){
						$tmp6->alt_image = $_POST['alt_image'.$key->id];

					}else{
						$old_nom=$this->Section1Model->getAltImageSection1($key->id);
						$altImagePath=$old_nom[0]->alt_image;
						$tmp6->alt_image = $altImagePath;
					}

					if(isset($_POST['text'.$key->id]) && strlen($_POST['text'.$key->id] ) > 0){
						$tmp6->text = $_POST['text'.$key->id];

					}else{
						$old_nom=$this->Section1Model->getTextSection1($key->id);
						$textPath=$old_nom[0]->text;
						$tmp6->text = $textPath;
					}

					if (isset($_FILES['imageUpdate'.$key->id]) && $_FILES['imageUpdate'.$key->id]['size'] > 0) {
						$upload_dir = "assets/images/section1";
						if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
						}
						$filepath = $upload_dir . '/' . $_FILES['imageUpdate'.$key->id]['name'];
						if (move_uploaded_file($_FILES['imageUpdate'.$key->id]["tmp_name"], $filepath)) {
							$tmp6->image_path = $filepath;
						}
					} else {
						$old_nom = $this->Section1Model->getImgSection1($key->id);
						$imgPath = $old_nom[0]->image_path;
						$tmp6->image_path = $imgPath;
					}

					$this->Section1Model->updatePartie2Section1($tmp6, $key->id);
					http_response_code(200);
				}
			}
			else {
				$data['afficheTitrePrincipaleSection1']=$this->GetSection1->afficheTitrePrincipaleSection1();
					$tmp = $this->Section1Model->getSection1();
					$tmp5= $this->Section1Model->getNotreEquipe();
					$data['notreEquipe']=$tmp5;
					$data['page'] = $tmp[0]; //Selection le premier élément du tableau
					$sousTitreSection= 'Notre équipe';
					$tmp2=$this->Section1Model->getSection1_exclu_titre($sousTitreSection); // Envoie au model le titre de la sous section
					$data['pageExcluTitre']=$tmp2; // Récupère le résultat de la fonction qui exclu le premier titre reçue du model , j'utilise une variable (tmp2) pour l'envoyer dans la vue
					$data['pages'] = 'pages/updateSection1';
					$this->load->view("templates/admin", $data);
			}
		}
		else{
			http_response_code(403);
		}
	}
	public function ajouteSection1(){
	session_start();
	if(isset($_SESSION['connected'])){
		if(isset($_FILES['imageAjoute']) && $_FILES['imageAjoute']['size'] > 0){
			$upload_dir="assets/images/section1";
			if(!file_exists($upload_dir)){
				mkdir($upload_dir,0777,true);
			}
			$filepath = $upload_dir . '/' . $_FILES['imageAjoute']['name'];
			if(move_uploaded_file($_FILES['imageAjoute']["tmp_name"], $filepath)){
				$titreAjoute = $_POST['titreAjoute'];
				$alt_imageAjoute= $_POST['alt_imageAjoute'];
				$textAjoute= $_POST['textAjoute'];
				$result = $this->Section1Model->addInSection1($titreAjoute,$alt_imageAjoute,$filepath,$textAjoute);
				if($result){
					http_response_code(200);
				}
				else{
					http_response_code(500);
				}
			}
			else{
				http_response_code(500);
			}
		}
		else{
			http_response_code(400);
		}
	}else{
		redirect('/');
	}
}
	public function deleteSection1()
	{
		session_start();
		if (isset($_SESSION['connected'])) {
			$id = $_POST['id'];
			if (isset($id)) {
				$this->Section1Model->deleteInSection1($id);
			}
		}
	}

	//-----fin de Update de la section "Qui sommes nous"-----\\

//-----début de Update de la sous-section "Notre équipe"-----\\

	public function ajouteSousSection1(){
		session_start();
		if(isset($_SESSION['connected'])){
			if(isset($_FILES['imageAjoute']) && $_FILES['imageAjoute']['size'] > 0){
				$upload_dir="assets/images/section1";
				if(!file_exists($upload_dir)){
					mkdir($upload_dir,0777,true);
				}
				$filepath = $upload_dir . '/' . $_FILES['imageAjoute']['name'];
				if(move_uploaded_file($_FILES['imageAjoute']["tmp_name"], $filepath)){

					$alt_imageAjoute= $_POST['alt_imageAjoute'];
					$textAjoute= $_POST['textAjoute'];
					$result = $this->Section1Model->addInSousSection1($alt_imageAjoute,$filepath,$textAjoute);
					if($result){
						http_response_code(200);
					}
					else{
						http_response_code(500);
					}
				}
				else{
					http_response_code(500);
				}
			}
			else{
				http_response_code(400);
			}
		}else{
			redirect('/');
		}
	}
//-----fin de Update de la sous-section "Notre équipe"-----\\

//-----début de Update de la section "présentation"-----\\
	public function updateSection2()
	{
		session_start();
		if (isset($_SESSION['connected'])) {

			if (isset($_POST['id'])) {
				$id = $_POST['id']; // on récupère l'id du formulaire
				$tmp = new stdClass();
				$tmp3 = new stdClass();
				$tmp->titre = $_POST['titre1']; // on récupère le titre du formulaire
				$tmp2 = $this->Section2Model->getSection2();
				$titreSection = 'Une réalisation de A a Z';
				$allDB = $this->Section2Model->getSection2_exclu_titre($titreSection);
				$allUneRealisationdeAaZ=$this->Section2Model->getUneRealisationdeAaZ();

				if (isset($_POST['titre1']) && strlen($_POST['titre1']) > 0){
					$tmp->titre = $_POST['titre1'];
				}else{
					$titreSection=$tmp2[0]->titre;
					$tmp->titre = $titreSection;
				}
				$this->Section2Model->updateTitreSection2($tmp, $id);

				foreach ($allDB as $key) {

					if(isset($_POST['alt_image'.$key->id]) && strlen($_POST['alt_image'.$key->id] ) > 0){
						$tmp->alt_image = $_POST['alt_image'.$key->id];

					}else{
						$old_nom=$this->Section2Model->getAltImageSection2($key->id);
						$altImagePath=$old_nom[0]->alt_image;
						$tmp->alt_image = $altImagePath;
					}
					if(isset($_POST['text'.$key->id]) && strlen($_POST['text'.$key->id] ) > 0){
						$tmp->text = $_POST['text'.$key->id];

					}else{
						$old_nom=$this->Section2Model->getTextSection2($key->id);
						$textPath=$old_nom[0]->text;
						$tmp->text = $textPath;
					}
					if (isset($_FILES['image' . $key->id]) && $_FILES['image' . $key->id]['size'] > 0) { // 'imgUpdate' correspond au nom du fihier uploader par le formulaire
						$upload_dir = "assets/images/section2/autres_realisations";
						if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
						}
						$filepath = $upload_dir . '/' . $_FILES['image' . $key->id]['name'];
						if (move_uploaded_file($_FILES['image' . $key->id]["tmp_name"], $filepath)) {
							$tmp->image_path = $filepath;
						}
					} else {
						$old_nom = $this->Section2Model->getImgSection2($key->id); // on récupère le chemin brut de l'image avant qu'il ne soit modifié on exécute la requete SQL
						$imgPath = $old_nom[0]->image_path; // on récupère la position du chemin de l'image dans le tableau qui est envoyé par la fonction d'update
						$tmp->image_path = $imgPath; // on stock la variable dans une variable temporaire ( stdClass) pour l'envoyer au model
					}
					$this->Section2Model->updatePartie1Section2($tmp, $key->id);

					http_response_code(200);
				}
				foreach ($allUneRealisationdeAaZ as $key){

					if(isset($_POST['alt_image'.$key->id]) && strlen($_POST['alt_image'.$key->id] ) > 0){
						$tmp3->alt_image = $_POST['alt_image'.$key->id];
					}else{
						$old_nom=$this->Section2Model->getAltImageSection2($key->id);
						$altImagePath=$old_nom[0]->alt_image;
						$tmp3->alt_image = $altImagePath;
					}
					if(isset($_POST['text'.$key->id]) && strlen($_POST['text'.$key->id] ) > 0){
						$tmp3->text = $_POST['text'.$key->id];

					}else{
						$old_nom=$this->Section2Model->getTextSection2($key->id);
						$textPath=$old_nom[0]->text;
						$tmp3->text = $textPath;
					}

					if (isset($_FILES['imageUpdate'.$key->id]) && $_FILES['imageUpdate'.$key->id]['size'] > 0) { // 'imgUpdate' correspond au nom du fichier uploader par le formulaire
						$upload_dir = "assets/images/section2/galerie";
						if (!file_exists($upload_dir)) {
							mkdir($upload_dir, 0777, true);
						}
						$filepath = $upload_dir . '/' . $_FILES['imageUpdate'.$key->id]['name'];
						if (move_uploaded_file($_FILES['imageUpdate'.$key->id]["tmp_name"], $filepath)) {
							$tmp3->image_path = $filepath;
						}
					} else {
						$old_nom = $this->Section2Model->getImgSection2($key->id); // on récupère le chemin brut de l'image avant qu'il ne soit modifié on exécute la requete SQL
						$imgPath = $old_nom[0]->image_path; // on récupère la position du chemin de l'image dans le tableau qui est envoyé par la fonction d'update
						$tmp3->image_path = $imgPath; // on stock la variable dans une variable temporaire ( stdClass) pour l'envoyer au model
					}
					$this->Section2Model->updatePartie2Section2($tmp3, $key->id);
					http_response_code(200);
				}

			} else {
				$tmp = $this->Section2Model->getSection2();
				$data['page'] = $tmp[0];
				$data['pages'] = 'pages/updateSection2';
				$titreAExclure = 'Une réalisation de A a Z';
				$tmp6 = $this->Section2Model->getSection2_exclu_titre($titreAExclure); // Envoie au model le titre de la sous section
				$data['premiereGallerie'] = $tmp6; // Récupère le résultat de la fonction qui exclu le premier titre reçue du model , j'utilise une variable (tmp2) pour l'envoyer dans la vue
				$tmp8= $this->Section2Model->getUneRealisationdeAaZ();
				$data['sousSection']=$tmp8;
				$this->load->view("templates/admin", $data);
			}
		} else {
			http_response_code(403);
		}
	}

		public function ajouteSection2(){
		session_start();
		if(isset($_SESSION['connected'])){
			if(isset($_FILES['imageAjoute']) && $_FILES['imageAjoute']['size'] > 0){

				$upload_dir="assets/images/section2";
				if(!file_exists($upload_dir)){
					mkdir($upload_dir,0777,true);
				}
				$filepath = $upload_dir . '/' . $_FILES['imageAjoute']['name'];
				if(move_uploaded_file($_FILES['imageAjoute']["tmp_name"], $filepath)){
					$alt_imageAjoute= $_POST['alt_imageAjoute'];
					$textAjoute= $_POST['textAjoute'];
					$result = $this->Section2Model->addInSection2($alt_imageAjoute,$filepath,$textAjoute);
					if($result){
						http_response_code(200);
					}
					else{
						http_response_code(500);
					}
				}
				else{
					http_response_code(500);
				}
			}
			else{
				http_response_code(400);
			}
		}else{
			redirect('/');
		}

	}

	//-----fin de la première partie de la section2-----\\

//-----début de Update de la sous-section "Une réalisation de A à Z"-----\\

	public function ajouteSousSection2(){
		session_start();
		if(isset($_SESSION['connected'])){
			if(isset($_FILES['imageAjoute']) && $_FILES['imageAjoute']['size'] > 0){

				$upload_dir="assets/images/section2";
				if(!file_exists($upload_dir)){
					mkdir($upload_dir,0777,true);
				}
				$filepath = $upload_dir . '/' . $_FILES['imageAjoute']['name'];
				if(move_uploaded_file($_FILES['imageAjoute']["tmp_name"], $filepath)){

					$alt_imageAjoute= $_POST['alt_imageAjoute'];
					$textAjoute= $_POST['textAjoute'];
					$result = $this->Section2Model->addInSousSection2($alt_imageAjoute,$filepath,$textAjoute);
					if($result){
						http_response_code(200);
					}
					else{
						http_response_code(500);
					}
				}
				else{
					http_response_code(500);
				}
			}
			else{
				http_response_code(400);
			}
		}else{
			redirect('/');
		}

	}


//-----fin de Update de la sous-section "Une réalisation de A à Z"-----\\


	public function deleteSection2()
	{
		session_start();
		if (isset($_SESSION['connected'])) {
			$id = $_POST['id'];
			if (isset($id)) {
				$this->Section2Model->deleteInSection2($id);
			}
		}
	}

//-----fin de Upadate de la section "réalisation"-----\\


//-----début de Update de la section "Votre projet"-----\\

	public function updateSection3()
	{
		session_start();
		if (isset($_SESSION['connected'])) {

			if(isset($_POST['id'])){
				$id = $_POST['id'];
				$tmp = new stdClass();
				$tmp->titre = $_POST['titre'];
				$tmp->alt_image = $_POST['alt_image'];
				$tmp->text = $_POST['text'];

				if(isset($_FILES['imgUpdate']) && $_FILES['imgUpdate']['size'] > 0){
					$upload_dir = "assets/images/section3";
					if (!file_exists($upload_dir)) {
						mkdir($upload_dir, 0777, true);
					}
					$filepath = $upload_dir . '/' . rand() . $_FILES['imgUpdate']['name'];
					if (move_uploaded_file($_FILES['imgUpdate']["tmp_name"], $filepath)) {
						$tmp->image_path = $filepath;
					}
				}
				else{
					$old_nom = $this->Section3Model->getImgSection3($id);
					$imgPath= $old_nom[0]->image_path;
					$tmp->image_path = $imgPath;
				}
				$this->Section3Model->updateSection3($tmp,$id);

				http_response_code(200);
			}
			else {
				$data['afficheVotreProjet']=$this->GetSection3->afficheVotreProjet();
				$tmp = $this->Section3Model->getSection3();
				$data['page'] = $tmp[0];
				$data['pages'] = 'pages/updateSection3';
				$this->load->view("templates/admin", $data);
			}
		}
		else{
			http_response_code(403);
		}
	}
//-----fin de Update de la section "Votre projet"-----\\

//-----début de Update de la section "Contactez Nous"-----\\

	public function updateContact()
	{
		session_start();
		if (isset($_SESSION['connected'])) {
			if(isset($_POST['id'])){
				$tmp = new stdClass();
				$allDb= $this->ContactModel->getContact();

				foreach ($allDb as $key) {
					$tmp->text = $_POST['text'.$key->id];
					$this->ContactModel->updateContact($tmp, $key->id);
					http_response_code(200);
				}
			}
			else {
				$tmp = $this->ContactModel->getContact();
				$data['page'] = $tmp[0];
				$data['contact']=$tmp;
				$data['pages'] = 'pages/updateContact';
				$this->load->view("templates/admin", $data);
			}
		}
		else{
			http_response_code(403);
		}
	}
//-----fin de Update de la section "Contactez nous"-----\\

//-----début de Update de la section "Footer"-----\\

	public function updateFooter()
	{
		session_start();
		if (isset($_SESSION['connected'])) {
			if(isset($_POST['id'])){
				$tmp = new stdClass();
				$allDb= $this->FooterModel->getFooter();

				foreach ($allDb as $key) {
					$tmp->text = $_POST['text'.$key->id];
					$tmp->lien = $_POST['lien'.$key->id];
					$this->FooterModel->updateFooter($tmp, $key->id);
					http_response_code(200);
				}
			}
			else {
				$tmp = $this->FooterModel->getFooter();
				$data['page'] = $tmp[0];
				$data['footer']=$tmp;
				$data['pages'] = 'pages/updateFooter';
				$this->load->view("templates/admin", $data);
			}
		}
		else{
			http_response_code(403);
		}
	}
//-----fin de Update de la section "Footer"-----\\

}

