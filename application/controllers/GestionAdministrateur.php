<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestionAdministrateur extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('GestionAdmin');
	}
	public function updateAdministrateur(){
		//vérification session
		session_start();
		if(isset($_SESSION['connected'])) {
			if(isset($_POST['id'])){
				$administrateur = new stdClass();
				$administrateur->old_password = $_POST['oldpassword'];
				$administrateur->new_password = $_POST['newpassword'];
				$administrateur->id = $_POST['id'];
				$administrateur->email = $_POST['email'];
				$result = $this->GestionAdmin->updateAdministrateur($administrateur);

				if($result){
					http_response_code(200);
				}
				else{
					http_response_code(403);
				}
			}
			else{
				$administrateur = $this->GestionAdmin->getAdministrateur();
				$administrateur = $administrateur[0];
				$data['administrateur'] = $administrateur;
				$data['pages'] = 'pages/updateAdmin';
				$this->load->view('templates/admin', $data);
			}
		}
		else{
			redirect('/');
		}
	}
}
