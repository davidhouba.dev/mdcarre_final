<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('GetHeader');
		$this->load->model('GetSection1');
		$this->load->model('GetSection2');
		$this->load->model('GetSection3');
		$this->load->model('GetContact');
		$this->load->library('form_validation');
		$this->load->model('GetFooter');
	}


	public function index(){

		$data['afficheImageHeader']=$this->GetHeader->afficheImageHeader();

		$data['afficheTitrePrincipaleSection1']=$this->GetSection1->afficheTitrePrincipaleSection1();
		$data['affichePremierePartieSection1']=$this->GetSection1->affichePremierePartieSection1();
		$data['afficheDeuximePartieSection1']=$this->GetSection1->afficheDeuximePartieSection1();

		$data['afficheTitrePrincipaleSection2']=$this->GetSection2->afficheTitrePrincipaleSection2();
		$data['affichePremierePartieSection2']=$this->GetSection2->affichePremierePartieSection2();
		$countModal=count($data['affichePremierePartieSection2']);
		$data['countModal']=$countModal;


		$data['afficheDeuxiemePartieSection2']=$this->GetSection2->afficheDeuxiemePartieSection2();
		$countModal2=count($data['afficheDeuxiemePartieSection2']);
		$data['countModal2']=$countModal2;

		$data['afficheVotreProjet']=$this->GetSection3->afficheVotreProjet();


		$data['afficheTextContact']=$this->GetContact->afficheTextContact();

		$data['afficheTextFooter']=$this->GetFooter->afficheTextFooter();


		$formData = array();

		// If contact request is submitted
		if($this->input->post('contactSubmit')){

			// Get the form data
			$formData = $this->input->post();

			// Form field validation rules
			$this->form_validation->set_rules('nom', 'Nom', 'required');
			$this->form_validation->set_rules('prenom', 'Prénom', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('tel', 'Téléphone');
			$this->form_validation->set_rules('sujet', 'Message', 'required');

			// Validate submitted form data
			if($this->form_validation->run() == true){

				// Define email data
				$mailData = array(
					'nom' => $formData['nom'],
					'prenom' => $formData['prenom'],
					'email' => $formData['email'],
					'tel' => $formData['tel'],
					'sujet' => $formData['sujet']
				);

				// Envoie un mail à l'administrateur
				$this->sendEmail($mailData);

			}
		}

		// Envoie les données à la vue
		$data['postData'] = $formData;

		$data['pages']='pages/index';
		$data['data']='';
		$this->load->view('templates/master',$data);
	}

	public function mentionsLegales(){
		$this->load->view('pages/mentionsLegales');
	}


	private function sendEmail($mailData){
		// Charge la librairie "email"
		$this->load->library('email');

		// Configuration du mail
		$to = 'info@mdcarre.be';
		$from = $mailData['email'];
		$fromName = 'MdCarre';
		$mailSubject = 'Demande de contact par '.$mailData['nom'];

		// Contenu du mail
		$mailContent = '
            <h2>Demande de prise de Contact</h2>
            <p><b>Nom: </b>'.$mailData['nom'].'</p>
            <p><b>Prénom: </b>'.$mailData['prenom'].'</p>
            <p><b>Email: </b>'.$mailData['email'].'</p>
            <p><b>Téléphone: </b>'.$mailData['tel'].'</p>
            <p><b>Message: </b>'.$mailData['sujet'].'</p>
        ';

		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->to($to);
		$this->email->from($from, $fromName);
		$this->email->subject($mailSubject);
		$this->email->message($mailContent);

		// Envoie l'email et recharge le site

		if($this->email->send() == true){
			redirect('pages/index');
		}


	}







}
