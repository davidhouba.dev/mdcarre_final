<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Section2Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getSection2(){
		return $this->db->select('*')->from('realisations')->get()->result();
	}

	public function getAltImageSection2($id){
		return $this->db->select('alt_image')->where('id', $id)->get('realisations')->result();
	}

	public function getImgSection2($id){
		return $this->db->select('image_path')->where('id', $id)->get('realisations')->result(); // on récupère le chemin de l'image
	}

	public function getTextSection2($id){
		return $this->db->select('text')->where('id', $id)->get('realisations')->result();
	}

	public function getSection2_exclu_titre($titreSection){
		$array = array('id !=' => '1', 'titre !=' => $titreSection); //l'utilisation de la variable "array" est une spécificité de codeigniteur pour indiquer plusieurs conditions
		return $this->db->select('*')->from('realisations')->where($array)->get()->result();
	}

	public function updateTitreSection2($tmp,$id){
			$this->db->set('titre',$tmp->titre)->where('id',$id)->update('realisations');
			return array();
	}

	public function updatePartie1Section2($tmp,$id){
		$this->db->set('titre','Nos réalisations')->set('image_path',$tmp->image_path)->set('text',$tmp->text)->set('alt_image',$tmp->alt_image)->where('id',$id)->update('realisations');
		return array();
	}

	public function updatePartie2Section2($tmp3,$id){
		$this->db->set('titre','Une réalisation de A à Z')->set('image_path',$tmp3->image_path)->set('text',$tmp3->text)->set('alt_image',$tmp3->alt_image)->where('id',$id)->update('realisations');
		return array();

	}

	public function addInSection2($alt_imageAjoute,$filepath,$textAjoute){
		$addDonnees = array(
			'titre'=>'Nos réalisations',
			'alt_image'=>$alt_imageAjoute,
			'image_path'=>$filepath,
			'text'=>$textAjoute);

		$this->db->insert('realisations',$addDonnees);
		if ($this->db->affected_rows() === 1){
			return true;
		}
		else{
			return false;
		}
	}

	public function getUneRealisationdeAaZ(){
		return $this->db->select('*')->from('realisations')->where('titre','Une réalisation de A à Z')->get()->result();
	}

	public function updateSection2($tmp,$id){
		$this->db->set('titre','Nos réalisations')->set('image_path',$tmp->image_path)->set('text',$tmp->text)->set('alt_image',$tmp->alt_image)->where('id',$id)->update('realisations');
		return array();
	}

	public function addInSousSection2($alt_imageAjoute,$filepath,$textAjoute){
		$addDonnees = array(
			'titre'=>'Une réalisation de A à Z',
			'alt_image'=>$alt_imageAjoute,
			'image_path'=>$filepath,
			'text'=>$textAjoute);

		$this->db->insert('realisations',$addDonnees);
		if ($this->db->affected_rows() === 1){
			return true;
		}
		else{
			return false;
		}
	}

	public function deleteInSection2($id){
		return $this->db->where('id',$id)->delete("realisations");
	}
}


