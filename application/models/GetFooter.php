<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetFooter extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function afficheTextFooter(){
		$query = $this->db->select('text,lien')
			->from('footer')
			->get();
		$afficheTextFooter = $query->result_array();
		return $afficheTextFooter;
	}



}
