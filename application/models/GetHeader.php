<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetHeader extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function afficheImageHeader(){
		$query = $this->db->select('*')
			->from('header')
			->get();
		$afficheImageHeader = $query->result_array();
		return $afficheImageHeader;
	}
}
