<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetSection1 extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function afficheTitrePrincipaleSection1(){
		$query = $this->db->select('titre')
			->from('qui_sommes_nous')
			->where('id',1)
			->get();
		$afficheTitrePrincipaleSection1 = $query->result_array();
		return $afficheTitrePrincipaleSection1;
	}

	public function affichePremierePartieSection1(){
		$array=array('id !=' => '1', 'titre !=' => 'Notre équipe');
		$query = $this->db->select('*')
			->from('qui_sommes_nous')
			->where($array)
			->get();
		$affichePremierePartieSection1 = $query->result_array();
		return $affichePremierePartieSection1;
	}

	public function afficheDeuximePartieSection1(){
		$array=array('id !=' => '1', 'titre =' => 'Notre équipe');
		$query = $this->db->select('*')
			->from('qui_sommes_nous')
			->where($array)
			->get();
		$afficheDeuximePartieSection1 = $query->result_array();
		return $afficheDeuximePartieSection1;
	}

}
