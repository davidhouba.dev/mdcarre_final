<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Section1Model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getSection1(){
		return $this->db->select('*')->from('qui_sommes_nous')->get()->result();
	}

	//Permet de récupérer tout les titre sauf le premier
	public function getSection1_exclu_titre($sousTitreSection){
		$array = array('id !=' => '1', 'titre !=' => $sousTitreSection);
		return $this->db->select('*')->from('qui_sommes_nous')->where($array)->get()->result();
	}
	public function getNotreEquipe(){
		return $this->db->select('*')->from('qui_sommes_nous')->where('titre','Notre équipe')->get()->result();
	}

	public function getImgSection1($id){
		return $this->db->select('image_path')->where('id', $id)->get('qui_sommes_nous')->result(); // on récupère le chemin de l'image
	}

	public function getSousTitreSection1($id){
		return $this->db->select('titre')->where('id', $id)->get('qui_sommes_nous')->result();
	}

	public function getAltImageSection1($id){
		return $this->db->select('alt_image')->where('id', $id)->get('qui_sommes_nous')->result();
	}

	public function getTextSection1($id){
		return $this->db->select('text')->where('id', $id)->get('qui_sommes_nous')->result();
	}

	public function updateTitreSection1($tmp,$id){
	$this->db->set('titre',$tmp->titre)->where('id',$id)->update('qui_sommes_nous');
	return array();
	}

	//La fonction récupère les différentes valeur des champs et met à jour à la db
	public function updatePartie1Section1($tmp,$id){
		$this->db->set('titre',$tmp->sousTitre)->set('image_path',$tmp->image_path)->set('text',$tmp->text)->set('alt_image',$tmp->alt_image)->where('id',$id)->update('qui_sommes_nous');
		return array();
	}

	public function updatePartie2Section1($tmp6,$id){
		$this->db->set('image_path',$tmp6->image_path)->set('text',$tmp6->text)->set('alt_image',$tmp6->alt_image)->where('id',$id)->update('qui_sommes_nous');
		return array();

	}



	public function addInSection1($titreAjoute,$alt_imageAjoute,$filepath,$textAjoute){
		$addDonnees = array(
			'titre'=>$titreAjoute,
			'alt_image'=>$alt_imageAjoute,
			'image_path'=>$filepath,
			'text'=>$textAjoute);

		$this->db->insert('qui_sommes_nous',$addDonnees);
		if ($this->db->affected_rows() === 1){
			return true;
		}
		else{
			return false;
		}
	}

	public function addInSousSection1($alt_imageAjoute,$filepath,$textAjoute){
		$addDonnees = array(
			'titre'=>'Notre équipe',
			'alt_image'=>$alt_imageAjoute,
			'image_path'=>$filepath,
			'text'=>$textAjoute);

		$this->db->insert('qui_sommes_nous',$addDonnees);
		if ($this->db->affected_rows() === 1){
			return true;
		}
		else{
			return false;
		}
	}

	public function deleteInSection1($id){
		return $this->db->where('id',$id)->delete("qui_sommes_nous");
	}
}
