<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ConnectionModel extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function logAdmin($email, $password){
		try{
			$password = hash('sha512',$password); // Fonction qui permet de crypter le mot de passe
			$results = $this->db
				->where(array(
					"password" => $password,
					"email" => $email
				))
				->get('administrateur')->result(); // Vérifie que l'email et le mot de passe correspondent à ce que la fonction reçoit
			if (count($results) == 1) {
				return true;
			}
			else{
				return false;
			}
		}
		catch(Exception $e){
			return $e->getMessage();
		}
	}

}
