<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HeaderModel extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getHeader(){
		return $this->db->select('*')->from('header')->get()->result();

	}

	public function getImgHeader($id){
		return $this->db->select('image_path')->where('id', $id)->get('header')->result(); // on récupère le chemin de l'image
	}
	public function updateImgHeader($tmp,$id){
		// on récupère les différents éléments à mettre à jour et on exécute la commande SQL pour le faire
		$this->db->set('image_path',$tmp->image_path)->set('alt_image',$tmp->alt_image)->where('id',$id)->update('header');
		return array();
	}



}
