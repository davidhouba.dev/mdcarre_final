<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetSection2 extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
	public function afficheTitrePrincipaleSection2(){
		$query = $this->db->select('titre')
			->from('realisations')
			->where('id',1)
			->get();
		$afficheTitrePrincipaleSection2 = $query->result_array();
		return $afficheTitrePrincipaleSection2;
	}

	public function affichePremierePartieSection2(){
		$array=array('id !=' => '1', 'titre !=' => 'Une réalisation de A à Z');
		$query = $this->db->select('*')
			->from('realisations')
			->where($array)
			->get();
		$affichePremierePartieSection2 = $query->result_array();
		return $affichePremierePartieSection2;
	}

	public function afficheDeuxiemePartieSection2(){
		$array=array('id !=' => '1', 'titre !=' => 'Nos réalisations');
		$query = $this->db->select('*')
			->from('realisations')
			->where($array)
			->get();
		$afficheDeuxiemePartieSection2 = $query->result_array();
		return $afficheDeuxiemePartieSection2;
	}


}
