<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ContactModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function GetContact(){
		return $this->db->select('*')->from('contactez_nous')->get()->result();
	}
	public function updateContact($tmp,$id){
		$this->db->set('text',$tmp->text)->where('id',$id)->update('contactez_nous');
		return array();
	}


}
