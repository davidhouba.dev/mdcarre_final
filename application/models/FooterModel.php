<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FooterModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function GetFooter(){
		return $this->db->select('*')->from('footer')->get()->result();
	}
	public function updateFooter($tmp,$id){
		$this->db->set('text',$tmp->text)->set('lien',$tmp->lien)->where('id',$id)->update('footer');
		return array();
	}


}
