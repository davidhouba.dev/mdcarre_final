<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetSection3 extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function afficheVotreProjet(){
		$query = $this->db->select('*')
			->from('votre_projet')
			->get();
		$afficheVotreProjet = $query->result_array();
		return $afficheVotreProjet;
	}
}
