<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Section3Model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getSection3(){
		return $this->db->select('*')->from('votre_projet')->get()->result();
	}

	public function getImgSection3($id){
		return $this->db->select('image_path')->where('id', $id)->get('votre_projet')->result(); // on récupère le chemin de l'image
	}
	public function updateSection3($tmp){
		// on récupère les différents éléments à mettre à jour et on exécute la commande SQL pour le faire
		$this->db->set('titre', $tmp->titre)->set('text', $tmp->text)->set('image_path',$tmp->image_path)->set('alt_image',$tmp->alt_image)->update('votre_projet');
		return array();
	}

}
