<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GestionAdmin extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function getAdministrateur(){
		return $this->db->select('*')->from('administrateur')->get()->result();
	}

	public function updateAdministrateur($administrateur){ // Cette fonction met à jour les données administrateur
		$password = hash('sha512',$administrateur->old_password);//Il stock l'ancien mot de passe dans une variable
		$this->db->where('id', $administrateur->id)->where('password', $password);
		if(isset($administrateur->email) && $administrateur->email != ''){ // il vérifie que la fonction reçoit bien un email et qu'il n'est pas vide
			$this->db->set('email', $administrateur->email);
		}
		if(isset($administrateur->new_password) && $administrateur->new_password != ''){// il vérifie que la fonction reçoit bien un mot de passe et qu'il n'est pas vide
			$this->db->set('password', hash('sha512',$administrateur->new_password));
		}
		$this->db->update('administrateur');// on met à jour la db
		$tmp = $this->db->affected_rows();
		if($tmp > 0){
			return true;
		}
		else{
			return false;
		}
	}

}
