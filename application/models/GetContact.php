<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GetContact extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function afficheTextContact(){
		$query = $this->db->select('text')
			->from('contactez_nous')
			->get();
		$afficheTextContact = $query->result_array();
		return $afficheTextContact;
	}



}
